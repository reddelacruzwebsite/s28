// S28 Activity Template:



// 1.) Insert a single room using the updateOne() method.
// Code here:

db.room.insertOne({
	"name": "single",
	"accommodates": 2,
	"price": 1000,
	"description": "A simple room with all the basic necessities",
	"rooms_available": 10,
	"isAvailable": false
})


// 2.) Insert multiple rooms using the insertMany() method.
// Code here:

db.room.insertMany([
{
	"name": "double",
	"accommodates": 3,
	"price": 2000,
	"description": "A room fit for a family going on a vacation",
	"rooms_available": 5,
	"isAvailable": false
},
{
	"name": "queen",
	"accommodates": 5,
	"price": 2000,
	"description": "Ideal for a grand vacation",
	"rooms_available": 2,
	"isAvailable": false
}
])

// 3.) Use find() method to search for a room with a name "double".
// Code here:

db.room.find({
	"name": "double"
})


// 4.) Use the updateOne() method to update the queen room and set the available rooms to 0.
// Code here:

db.room.updateOne({
	"name": "queen"
}, 
{
	$set: {
		"rooms_available": 0,
	}
}

)


// 5.) Use the deleteMany method to delete all rooms that have 0 rooms avaialable.
// Code here:


db.room.deleteMany({
	"rooms_available": 0
})
















